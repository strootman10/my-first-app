package com.example.masonry;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements CustomAdapter.ListItemClick{

    // ArrayList for person name
    private Toast mToast;

    ArrayList babysNames = new ArrayList<>(Arrays.asList("Person 1", "Person 2", "Person 3", "Person 4", "Person 5", "Person 6", "Person 7", "Person 8","Person 9", "Person 10"));
    ArrayList babysImages = new ArrayList<>(Arrays.asList(R.drawable.baby_one, R.drawable.baby_two, R.drawable.baby_three, R.drawable.baby_four, R.drawable.baby_five, R.drawable.baby_six, R.drawable.baby_seven, R.drawable.baby_eight, R.drawable.baby_nine, R.drawable.baby_ten ));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get the reference of RecyclerView
        RecyclerView recyclerView = findViewById(R.id.recyclerView);

        // Set a StaggeredGrindLayoutManager with 2 Numbers of columns and vertical orientation
        StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, LinearLayout.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager); // Set layoutManager to recyclerView

        CustomAdapter customAdapter = new CustomAdapter(babysNames, babysImages, this, this);
        recyclerView.setAdapter(customAdapter); // set the Adapter to RecyclerView


    }

    @Override
    public void onListClick(int clickedItem) {
        String msmToast = "Item :" + clickedItem + "Clicked";
        String nameOnlybaby = "His name is: " + babysNames.get(clickedItem);
        Integer imageOnlyBaby = (Integer) babysImages.get(clickedItem);
        mToast = Toast.makeText(this, msmToast + nameOnlybaby + " ", Toast.LENGTH_LONG);
        mToast.show();

        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("setnamebaby", nameOnlybaby);
        intent.putExtra("imagesrc", imageOnlyBaby);
        startActivity(intent);

    }
}
