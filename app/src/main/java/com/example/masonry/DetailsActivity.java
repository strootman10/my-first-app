package com.example.masonry;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    TextView textView;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        textView = findViewById(R.id.getNameBaby);
        imageView = findViewById(R.id.getImageBaby);

        //get the current intent
        Intent intent = getIntent();
//get the attached extras from the intent
//we should use the same key as we used to attach the data.
        String user_name = intent.getStringExtra("setnamebaby");
        String user_image = intent.getStringExtra("imagesrc");

        int intValue = intent.getIntExtra("imagesrc", 0);

        textView.setText(user_name + " " + intValue +" stroot");
        imageView.setImageResource(intValue);

    }
}
