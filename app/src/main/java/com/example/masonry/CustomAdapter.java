package com.example.masonry;

import android.content.Context;
import android.content.Intent;
import android.content.MutableContextWrapper;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder>{

    ArrayList babysNames;
    ArrayList babysImages;
    Context context;

    final private ListItemClick mOnClickLisytener;

    public CustomAdapter(ArrayList babysnames, ArrayList babysImages, Context context, ListItemClick listener) {
        this.babysNames = babysnames;
        this.babysImages = babysImages;
        this.context = context;
        mOnClickLisytener = listener;

    }

    public interface ListItemClick{
        void onListClick(int clickedItem);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        Context mContext = viewGroup.getContext();
        int layoutIdForListItem = R.layout.rowlayout;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        boolean attachToParentFast = false;
        View view = inflater.inflate(layoutIdForListItem,viewGroup, attachToParentFast);

        MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {
        // set the data in items

        myViewHolder.bind(position);

    }

    @Override
    public int getItemCount() {
        return babysNames.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        // Init the items view's

        TextView name;
        ImageView image;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            // Get the reference of item view's
            name = itemView.findViewById(R.id.name);
            image = itemView.findViewById(R.id.image);

            itemView.setOnClickListener(this);
        }

        void bind(int listIndex){
            name.setText(String.valueOf(babysNames.get(listIndex)));
            image.setImageResource((Integer) babysImages.get(listIndex));
        }

        @Override
        public void onClick(View v) {
            int clickedItem = getAdapterPosition();
            mOnClickLisytener.onListClick(clickedItem);
        }
    }
}
